#!/usr/bin/python2.7

import telepot
import sys
from motion_picam_creds import *

bot = telepot.Bot(bot_id)

pic = sys.argv[1]

# change caption if it is a snapshot or motion
if pic.endswith("snapshot.jpg"):
    cap = 'snapshot'
else:
    cap = 'motion detected'
for chat in chats_ids:
    bot.sendPhoto(chat, photo=open(pic, 'rb'), caption=cap)

exit(0)

