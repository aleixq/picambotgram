#!/usr/bin/python2.7

import datetime
import telepot
import time
import requests
import os
import glob
import socket
from requests.exceptions import ConnectionError
from motion_picam_creds import *
from telepot.loop import MessageLoop
from telepot.namedtuple import ReplyKeyboardMarkup, KeyboardButton, ForceReply, ReplyKeyboardRemove


def webcontrol(chat_id, type, cmd):
    try: 
        req = 'http://localhost:8080/0/'+type+'/'+cmd
        res = requests.get(req)
        text = res.text
    except ConnectionError as err:
        text =  "error: {0}".format(err)
    bot.sendMessage(chat_id, text)

def handle(msg):
    global end_confirming
    global clear_confirming
    chat_id = msg['chat']['id']
    command = msg['text']
    #should work thanks to Winston
    if msg['from']['id'] not in chats_ids:
        bot.sendMessage(chat_id, "Sorry this is a personal bot. Access Denied!")
        exit(1)
    
    if clear_confirming:
        clear_confirming = False
        if (command == "Confirm" or command.startswith('/Confirm@')):
            file_count = 0
            files_size = 0
            for file in glob.iglob( motion_path + '/*.*'):
                if not os.path.islink(file):
                    file_count += 1
                    files_size += os.path.getsize(file)
                    os.remove(file)
            for file in glob.iglob( motion_path + '/vids/*.*'):
                if not os.path.islink(file):
                    file_count += 1
                    files_size += os.path.getsize(file)
                    os.remove(file)
            bot.sendMessage(chat_id, 'Motion Path cleared, deleted ' + str(file_count) + ' files: ' + str(files_size/1024/1024) + ' Mbytes')
        bot.sendMessage(chat_id, 'Tell me commands!', reply_markup=ReplyKeyboardRemove())
        return
    if end_confirming :
        end_confirming = False
        if (command == "Confirm" or command.startswith('/Confirm@')):
            webcontrol(chat_id, 'action', 'end')
        bot.sendMessage(chat_id, 'Tell me commands!', reply_markup=ReplyKeyboardRemove())
        return

    print 'Got command: %s' % command

    if command == '/snapshot' or command.startswith('/snapshot@'):
        requests.get('http://localhost:8080/0/action/snapshot')
    elif command == '/status' or command.startswith('/status@'):
        webcontrol(chat_id, 'detection', 'status')
    elif command == '/pause' or command.startswith('/pause@'):
        webcontrol(chat_id, 'detection', 'pause')
    elif command == '/resume' or command.startswith('/resume@'):
        webcontrol(chat_id, 'detection', 'start')
    elif command == '/check' or command.startswith('/check@'):
        webcontrol(chat_id, 'detection', 'connection')
    elif command == '/endmotion' or command.startswith('/endmotion@'):
        markup = ReplyKeyboardMarkup(keyboard=[[KeyboardButton(text='Confirm')], [KeyboardButton(text='Cancel')]], one_time_keyboard=True)
        bot.sendMessage(chat_id, 'End?', reply_markup=markup)
        end_confirming = True
        webcontrol(chat_id, 'action', 'end')
    elif command == '/time' or command.startswith('/time@'):
        bot.sendMessage(chat_id, 'now is '+str(datetime.datetime.now()))
    elif command == '/preview' or command.startswith('/preview@'):
        bot.sendMessage(chat_id, 'now is ' + ext_ip + ':8081/ '+str(datetime.datetime.now()))
    elif command == '/clear' or command.startswith('/clear@'):
        markup = ReplyKeyboardMarkup(keyboard=[[KeyboardButton(text='Confirm')], [KeyboardButton(text='Cancel')]], one_time_keyboard=True)
        bot.sendMessage(chat_id, 'Clear?', reply_markup=markup)
        clear_confirming = True
    elif command == '/video' or command.startswith('/video@'):
        # the most recent video in this particular folder of complete vids
        video = max(glob.iglob( motion_path + '/vids/*.mp4'), key=os.path.getctime)
        # send video, adapt the the first argument to your own telegram id
        bot.sendVideo(chat_id, video=open(video, 'rb'), caption='last video')
    else:
        bot.sendMessage(chat_id, "sorry, I don't know the command "+command)

bot = telepot.Bot(bot_id)

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect((gateway, 80))
ext_ip = s.getsockname()[0]

end_confirming = False
clear_confirming = False

help_commands = """preview - show url of live preview
time - Returns current time on pi
check - Returns status of the camera
status - Returns status of motion
pause - Pauses the motion detection
resume - Resumes motion detection
snapshot - Returns current image
video - Returns last recorded video
clear - Clears the saved files from storage
endmotion - Entirely shutdown the Motion application"""

MessageLoop(bot, handle).run_as_thread()
print 'I am listening ...'

while 1:
    time.sleep(10)

